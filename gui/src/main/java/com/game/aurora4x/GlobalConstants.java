package com.game.aurora4x;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GlobalConstants {
  public static final String OS_NAME = "os.name";
  public static boolean isWindows = System.getProperty(OS_NAME).contains("Windows");
  public static boolean isLinux = System.getProperty(OS_NAME).contains("Linux");
  public static boolean isMac = System.getProperty(OS_NAME).contains("Mac");
  public static boolean isIos = false;
  public static boolean isAndroid = false;
  public static final String OS_ARCH = "os.arch";
  public static boolean isARM = System.getProperty(OS_ARCH).startsWith("arm");
  public static boolean is64Bit = System.getProperty(OS_ARCH).equals("amd64")
      || System.getProperty(OS_ARCH).equals("x86_64");

  // JDK 8 only.
  public static String abi = (System.getProperty("sun.arch.abi") != null ? System.getProperty("sun.arch.abi") : "");
}
