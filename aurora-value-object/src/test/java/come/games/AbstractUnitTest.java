package come.games;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.validation.Validation;
import javax.validation.Validator;

public abstract class AbstractUnitTest {

  @Rule
  public final ExpectedException thrown = ExpectedException.none();

  @Rule
  public MockitoRule rule = MockitoJUnit.rule();

  protected final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

}
