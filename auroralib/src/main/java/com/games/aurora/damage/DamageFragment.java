package com.games.aurora.damage;

import java.awt.Point;
import javax.vecmath.Vector2d;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DamageFragment {
  private Vector2d velocity;
  private Point position;
  private float mass;
  private float density;//kg/m^3
  private float length;
}
