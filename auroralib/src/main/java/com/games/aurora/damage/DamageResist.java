package com.games.aurora.damage;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DamageResist {
  /*
          this could potentialy get more complex,
          ie transperency, damage passes through but does no damage, probibly need that.
          absorption, and threashold - can absorb heat to a melting point.
          relfection/resistance - will compleatly ignore/reduce damage under a value
         */
  private byte idCode;
  private int hitPoints;
  private float heat;
  private float kinetic;
  private float density;
}
