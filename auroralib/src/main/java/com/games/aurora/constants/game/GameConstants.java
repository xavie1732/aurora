package com.games.aurora.constants.game;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GameConstants {
  final int MINIMUM_TIME_STEP = 5;
}
