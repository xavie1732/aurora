package com.games.aurora.staticdata;

import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class Cargoable {

  private UUID id;
  private String name;
  private UUID cargoTypeID;

  /// The smallest unit mass. 1 for most minerals etc.
  private int massPerUnit;

  private double volumePerUnit;
}
