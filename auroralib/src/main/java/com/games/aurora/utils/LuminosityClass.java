package com.games.aurora.utils;

public enum LuminosityClass {
  O,          // Hypergiants
  Ia,         // Luminous Supergiants
  Iab,        // Intermediate Supergiants
  Ib,         // Less Luminous Supergiants
  II,         // Bright Giants
  III,        // Giants
  IV,         // Subgiants
  V,          // Main-Sequence (like our sun)
  sd,         // Subdwarfs
  D,          // White Dwarfs
}
