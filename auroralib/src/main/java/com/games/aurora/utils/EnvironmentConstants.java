package com.games.aurora.utils;


import lombok.experimental.UtilityClass;

@UtilityClass
public class EnvironmentConstants {

  public final String DEVELOPMENT = "dev";

  public final String TESTING = "localTest";

  public final String TEST = "test";

  public final String SYST = "syst";

  public final String PRODUCTION = "prod";

}
