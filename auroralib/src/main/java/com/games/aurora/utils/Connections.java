package com.games.aurora.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  Connections {
  SKIN((byte) 1),
  FRONT((byte) 10),
  BACK((byte) 100),
  SIDS((byte) 1000),
  STRUCTURAL((byte) 10000);

  private byte value;
}
