package com.games.aurora.utils;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;


@Getter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class BaseData<T> implements Cloneable {
  /// <summary>
  /// This is the Entity which Owns/Conatains/IsParentOf this datablob
  /// </summary>
  @NonNull
  private final T id;


  public Object clone() throws CloneNotSupportedException {
     return super.clone();
  }
}
