package com.games.aurora.utils;

public enum SpectralType {
  O,
  B,
  A,
  F,
  G,
  K,
  M,
  D,
  C
}
