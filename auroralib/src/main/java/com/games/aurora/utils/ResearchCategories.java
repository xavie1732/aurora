package com.games.aurora.utils;

public enum ResearchCategories {
  BiologyGenetics,
  ConstructionProduction,
  DefensiveSystems,
  EnergyWeapons,
  LogisticsGroundCombat,
  MissilesKineticWeapons,
  PowerAndPropulsion,
  SensorsAndFireControl,
  FromStaticData00,
  FromStaticData01,
  FromStaticData02,
  FromStaticData03,
  FromStaticData04,
  FromStaticData05,
  FromStaticData06,
  FromStaticData07,
  FromStaticData08,
  FromStaticData09,
}
