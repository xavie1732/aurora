package com.games.aurora.utils;

import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public abstract class UUIDBasedValueObject implements Serializable {

  private static final long serialVersionUID = 1L;

  @NonNull
  private final UUID value;

  @JsonValue
  public String asString() {
    return value.toString();
  }

  public UUID asUuid() {
    return value;
  }

  @Override
  public String toString() {
    return asString();
  }

  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }
}
