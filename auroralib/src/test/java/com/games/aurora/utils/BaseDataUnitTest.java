package com.games.aurora.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;
import org.junit.Test;

public class BaseDataUnitTest {

  @Test(expected = NullPointerException.class)
  public void whenIdIsNull_thenThrowError(){
    new BaseData<String>(null);
  }

  @Test
  public void whenIdIsNotNull_thenReturnObject(){
    BaseData baseData = new BaseData(UUID.randomUUID());
    assertNotNull(baseData);
  }

  @Test
  public void whenIdIsNotNull_thenCheckClone() throws CloneNotSupportedException {
    BaseData<String> baseData = new BaseData<String>(UUID.randomUUID().toString());
    BaseData<String> clone = (BaseData<String>) baseData.clone();
    assertEquals(baseData, clone);
  }
}
