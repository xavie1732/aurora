package com.games.aurora.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ConnectionsUnitTest {

  @Test
  public void whenConnectionsAreAdded_thenReturnSome(){
    int testData = Connections.BACK.getValue() + Connections.SKIN.getValue();
    assertThat(testData).isEqualTo(101);
  }
}
