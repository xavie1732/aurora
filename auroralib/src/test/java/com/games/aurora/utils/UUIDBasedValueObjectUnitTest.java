package com.games.aurora.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.Test;

public class UUIDBasedValueObjectUnitTest {

  @Test(expected = NullPointerException.class)
  public void withNullValue_thenThrowError() {
    new UUIDBasedValueObjectImpl(null);
  }

  @Test
  public void withUUIDValue_thenCreateObject() {
    // prepare
    // act
    UUIDBasedValueObject uuidBasedValueObject = new UUIDBasedValueObjectImpl(UUID.randomUUID());
    // check
    assertThat(uuidBasedValueObject).isNotNull();
    assertThat(uuidBasedValueObject.asString()).isNotBlank();
    assertThat(uuidBasedValueObject.asUuid()).isNotNull();
    assertThat(uuidBasedValueObject.toString()).isNotNull();
  }

  @Test
  public void withUUIDValue_thenGetToString() {
    // prepare
    // act
    UUIDBasedValueObject uuidBasedValueObject = new UUIDBasedValueObjectImpl(UUID.randomUUID());
    // check
    assertThat(uuidBasedValueObject.toString()).isNotNull();
    assertThat(uuidBasedValueObject.toString()).isNotBlank();
  }

  @Test
  public void withUUIDValue_thenGetEqual() {
    // prepare
    // act
    UUIDBasedValueObject uuidBasedValueObject = new UUIDBasedValueObjectImpl(UUID.randomUUID());
    UUIDBasedValueObject uuidBasedValueObject1 = new UUIDBasedValueObjectImpl(UUID.randomUUID());
    // check
    assertThat(uuidBasedValueObject.equals(uuidBasedValueObject)).isTrue();
    assertThat(uuidBasedValueObject.equals(uuidBasedValueObject1)).isFalse();
  }

  @Test
  public void withUUIDValue_thenGetHashCode() {
    // prepare
    // act
    UUIDBasedValueObject uuidBasedValueObject = new UUIDBasedValueObjectImpl(UUID.randomUUID());
    // check
    assertThat(uuidBasedValueObject.hashCode()).isNotNull();
    assertThat(uuidBasedValueObject.hashCode()).isNotNegative();
  }


  private class UUIDBasedValueObjectImpl extends UUIDBasedValueObject {

    protected UUIDBasedValueObjectImpl(UUID value) {
      super(value);
    }
  }

}
